import harness.sx10Test;
import x10.util.Team;

public class Reduce extends sx10Test {

    def reduceTest(team:Team, role:int, root:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count, (i:int)=>((role+1) as Double) * i * i);
        val dst = new Array[Double](count, (i:int)=>-(i as Double));
        var success: boolean = true;
                
        {
            team.reduce(role, root, src, 0, dst, 0, count, Team.ADD);

            if (role == root) {
                val oracle_base = ((team.size()*team.size() + team.size())/2) as Double;
                for ([i] in 0..(count-1)) {
                    val oracle:double = oracle_base * i * i;
                    if (dst(i) != oracle) {
                        success = false;
                    }
                }
            }
        }

        {
            team.reduce(role, root, src, 0, dst, 0, count, Team.MAX);

            if (role == root) {
                val oracle_base = (team.size()) as Double;
                for ([i] in 0..(count-1)) {
                    val oracle:double = oracle_base * i * i;
                    if (dst(i) != oracle) {
                        success = false;
                    }
                }
            }
        }

        {
            team.reduce(role, root, src, 0, dst, 0, count, Team.MIN);

            if (role == root) {
                val oracle_base = 1.0f;
                for ([i] in 0..(count-1)) {
                    val oracle:double = oracle_base * i * i;
                    if (dst(i) != oracle) {
                        success = false;
                    }
                }
            }
        }

        val reducedSuccess = team.reduce(role, root, success ? 1 : 0, Team.AND);

        team.barrier(role);

        if (role == root) success &= (reducedSuccess == 1);

        if (!success) at (res.home) res().set(false);

    }

    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) reduceTest(Team.WORLD, here.id, 0,gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Reduce().execute();
    }

}

