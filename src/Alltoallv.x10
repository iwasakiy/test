import harness.sx10Test;
import x10.util.Team;

public class Alltoallv extends sx10Test {

    def alltoallvTest(team:Team, role:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count * team.size(), (i:int)=>((role+1) as Double) * i * i);
        val src_offs = new Array[int](team.size(), (i:int)=>count * i);
        val src_counts = new Array[int](team.size(), count);
        val dst = new Array[Double](count * team.size(), (i:int)=>-(i as Double));
        val dst_offs = new Array[int](team.size(), (i:int)=>count * i);
        val dst_counts = new Array[int](team.size(), count);
        var success: boolean = true;

        team.alltoallv(role, src, src_offs, src_counts, dst, dst_offs, dst_counts);
        for ([i] in 0..(team.size()-1)) {
            val oracle_base = (i + 1) as Double;
            for ([j] in 0..(count-1)) {
                val oracle:double = oracle_base * (j + role * count) * (j + role * count);
                if (dst(i * count + j) != oracle) {
                    success = false;
                }
            }
        }
                
        team.barrier(role);

        if (!success) at (res.home) res().set(false);

    }
    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) alltoallvTest(Team.WORLD, here.id, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Alltoallv().execute();
    }

}

