import harness.sx10Test;
import x10.util.Team;

public class Bcast extends sx10Test {

    def bcastTest(team:Team, role:int, root:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count, (i:int)=>((role+1) as Double) * i * i);
        val dst = new Array[Double](count, (i:int)=>-(i as Double));
        var success: boolean = true;

        team.bcast(role, root, src, 0, dst, 0, count);
        val oracle_base = 1.0f;
        for ([i] in 0..(count-1)) {
            val oracle:double = oracle_base * i * i;
            if (dst(i) != oracle) {
                success = false;
            }
        }
                
        team.barrier(role);

        if (!success) at (res.home) res().set(false);

    }
    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) bcastTest(Team.WORLD, here.id, 0, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Bcast().execute();
    }

}
