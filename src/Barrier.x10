import harness.sx10Test;
import x10.util.Team;

public class Barrier extends sx10Test {

    def barrierTest(team:Team, role:int, res:GlobalRef[Cell[Boolean]]) {
        var success: boolean = true;
                
        team.barrier(role);

        if (!success) at (res.home) res().set(false);

    }
    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) barrierTest(Team.WORLD, here.id, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Barrier().execute();
    }

}
