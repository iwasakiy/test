import harness.sx10Test;
import x10.util.Team;

public class Gather extends sx10Test {

    def gatherTest(team:Team, role:int, root:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count, (i:int)=>((role+1) as Double) * i * i);
        val dst = new Array[Double](count * team.size(), (i:int)=>-(i as Double));
        var success: boolean = true;

        team.gather(role, root, src, 0, dst, 0, count);
        if (role == root) {
            for ([i] in 0..(team.size()-1)) {
                val oracle_base = (i + 1) as Double;
                for ([j] in 0..(count-1)) {
                    val oracle:double = oracle_base * j * j;
                    if (dst(i * count + j) != oracle) {
                        success = false;
                    }
                }
            }
        }
                
        team.barrier(role);

        if (!success) at (res.home) res().set(false);

    }
    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) gatherTest(Team.WORLD, here.id, 0, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Gather().execute();
    }

}

