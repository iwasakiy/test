import harness.sx10Test;
import x10.util.Team;

public class Gatherv extends sx10Test {

    def gathervTest(team:Team, role:int, root:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count, (i:int)=>((role+1) as Double) * i * i);
        val dst = new Array[Double](count * team.size(), (i:int)=>-(i as Double));
        val dst_offs = new Array[int](team.size(), (i:int)=>count * i);
        val dst_counts = new Array[int](team.size(), count);
        var success: boolean = true;

        team.gatherv(role, root, src, 0, count, dst, dst_offs, dst_counts);
        if (role == root) {
            for ([i] in 0..(team.size()-1)) {
                val oracle_base = (i + 1) as Double;
                for ([j] in 0..(count-1)) {
                    val oracle:double = oracle_base * j * j;
                    if (dst(i * count + j) != oracle) {
                        success = false;
                    }
                }
            }
        }
                
        team.barrier(role);

        if (!success) at (res.home) res().set(false);

    }
    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) gathervTest(Team.WORLD, here.id, 0, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new Gatherv().execute();
    }

}

