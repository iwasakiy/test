import harness.sx10Test;
import x10.util.Team;

public class AllReduce extends sx10Test {

    def allReduceTest(team:Team, role:int, res:GlobalRef[Cell[Boolean]]) {
        val count = 113;        
        val src = new Array[Double](count, (i:int)=>((role+1) as Double) * i * i);
        val dst = new Array[Double](count, (i:int)=>-(i as Double));
        var success: boolean = true;
                
        {
            team.allreduce(role, src, 0, dst, 0, count, Team.ADD);

            val oracle_base = ((team.size()*team.size() + team.size())/2) as Double;
            for ([i] in 0..(count-1)) {
                val oracle:double = oracle_base * i * i;
                if (dst(i) != oracle) {
                    success = false;
                }
            }
        }

        {
            team.allreduce(role, src, 0, dst, 0, count, Team.MAX);

            val oracle_base = (team.size()) as Double;
            for ([i] in 0..(count-1)) {
                val oracle:double = oracle_base * i * i;
                if (dst(i) != oracle) {
                    success = false;
                }
            }
        }

        {
            team.allreduce(role, src, 0, dst, 0, count, Team.MIN);

            val oracle_base = 1.0f;
            for ([i] in 0..(count-1)) {
                val oracle:double = oracle_base * i * i;
                if (dst(i) != oracle) {
                    success = false;
                }
            }
        }

        val reducedSuccess = team.allreduce(role, success ? 1 : 0, Team.AND);

        team.barrier(role);

        success &= (reducedSuccess == 1);

        if (!success) at (res.home) res().set(false);

    }

    public def run(): boolean {
        val res:Cell[Boolean] = new Cell[Boolean](true);
        val gr:GlobalRef[Cell[Boolean]] = GlobalRef[Cell[Boolean]](res);
        
        finish for (p in Place.places()) {
            async at(p) allReduceTest(Team.WORLD, here.id, gr);
        }
        
        return res();
    }

    public static def main(args: Array[String]) {
        new AllReduce().execute();
    }

}
